/*
 * Copyright (c) 2010, Oracle. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice,
 *   this list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of Oracle nor the names of its contributors
 *   may be used to endorse or promote products derived from this software without
 *   specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
package erc;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Print extends javax.swing.JFrame {

    protected static Boolean resultPrint = false;

    protected static String AdobeReader = null;

    /**
     * Creates new form Find
     */
    public Print() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDialogPathInvoice = new javax.swing.JFileChooser();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextFilePrint = new javax.swing.JTextPane();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Печать счетов-извещений");

        jDialogPathInvoice.setDialogType(javax.swing.JFileChooser.CUSTOM_DIALOG);
        jDialogPathInvoice.setApproveButtonText("Печать");
        jDialogPathInvoice.setApproveButtonToolTipText("Отправка файлов на печать");
        jDialogPathInvoice.setCurrentDirectory(null);
        jDialogPathInvoice.setDialogTitle("Выберите каталог счетов-извещений");
        jDialogPathInvoice.setFileHidingEnabled(true);
        jDialogPathInvoice.setFileSelectionMode(javax.swing.JFileChooser.FILES_AND_DIRECTORIES);
        jDialogPathInvoice.setToolTipText("");
        jDialogPathInvoice.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jDialogPathInvoice.setMultiSelectionEnabled(true);
        jDialogPathInvoice.setName(""); // NOI18N
        jDialogPathInvoice.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jDialogPathInvoiceActionPerformed(evt);
            }
        });

        jTextFilePrint.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jScrollPane1.setViewportView(jTextFilePrint);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Файл на печати:");

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jDialogPathInvoice, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .add(jLabel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(18, 18, 18)
                .add(jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 680, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jScrollPane1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 36, Short.MAX_VALUE)
                    .add(jLabel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .add(18, 18, 18)
                .add(jDialogPathInvoice, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 397, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(59, 59, 59))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jDialogPathInvoiceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jDialogPathInvoiceActionPerformed
        // TODO add your handling code here:
        if ("ApproveSelection".equals(evt.getActionCommand())) {
            resultPrint = true;
            try {
                printInvoice();
            } catch (Exception ex) {
                Logger.getLogger(Print.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            resultPrint = false;
        }
    }//GEN-LAST:event_jDialogPathInvoiceActionPerformed

    /**
     * Печать инвойсов
     */
    private void printInvoice() throws Exception {
        File[] InvoiceList = jDialogPathInvoice.getSelectedFiles();
        printFileList(InvoiceList);
    }

    /**
     * Обход директории и печать файла
     *
     * @param fileList
     */
    private void printFileList(File[] fileList) throws Exception {
        for (File f : fileList) {
            if (f.isFile()) {
                String lowercaseName = f.getName().toLowerCase();
                if (!lowercaseName.endsWith(".pdf")) {
                    continue;
                }
                System.out.println(f.getPath());
                printPdf(f.getPath());
                displayCurrentFile(f.getName());
            } else if (f.isDirectory()) {
                printFileList(f.listFiles());
            }
        }
    }

    /**
     * Отображение текущего файла на печати
     *
     * @param f
     */
    private void displayCurrentFile(String f) {
        jTextFilePrint.setText(f);
    }

    /**
     * Печать pdf-файла
     *
     * @param file
     * @throws java.lang.Exception
     */
    public static void printPdf(String file) throws Exception {
        String commandAdobeReader = AdobeReader.concat(" /N /T ").concat(file);
        System.out.println(commandAdobeReader);
        Runtime.getRuntime().exec(commandAdobeReader);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) throws Exception {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            javax.swing.UIManager.LookAndFeelInfo[] installedLookAndFeels = javax.swing.UIManager.getInstalledLookAndFeels();
            for (int idx = 0; idx < installedLookAndFeels.length; idx++) {
                if ("Nimbus".equals(installedLookAndFeels[idx].getName())) {
                    javax.swing.UIManager.setLookAndFeel(installedLookAndFeels[idx].getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Print.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Print.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Print.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Print.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Print().setVisible(true);
            }
        });

        getAdobeReader();
    }

    /**
     * Определяем, где находится Adobe Reader
     *
     * @throws java.lang.Exception
     */
    public static void getAdobeReader() throws Exception {
        String[] paths = new String[]{
            "C:\\Program Files (x86)\\Adobe\\Acrobat Reader DC\\Reader\\AcroRd32.exe",
            "C:\\Program Files\\Adobe\\Acrobat Reader DC\\Reader\\AcroRd32.exe",
            "C:\\Program Files (x86)\\Adobe\\Acrobat Reader\\Reader\\AcroRd32.exe",
            "C:\\Program Files\\Adobe\\Acrobat Reader\\Reader\\AcroRd32.exe",
            "C:\\Program Files\\Adobe\\Reader 11.0\\Reader\\AcroRd32.exe",
            "C:\\Program Files (x86)\\Adobe\\Reader 11.0\\Reader\\AcroRd32.exe"
        };

        for (String path : paths) {
            File f = new File(path);
            if (f.exists()) {
                AdobeReader = path;
                break;
            }
        }

        if (AdobeReader == null) {
            throw new Exception("Not found Adobe Reader");
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JFileChooser jDialogPathInvoice;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextPane jTextFilePrint;
    // End of variables declaration//GEN-END:variables
}
